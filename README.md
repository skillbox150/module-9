# Модуль 9 

Автор   : eshlygin (shen22@rambler.ru)

Создано : 2022.11.20

---

## Особенности:

1. Переписан для использования в Яндекс облаке

2. Модульная структура Terraform 

3. Автоматически создает инвентори файлы для Ansible из шаблонов

4. Папки project_path и release path создаются в одной задаче с использованием цикла

5. IP React сервера для nginx определяется автоматически из инвентори файла.

## Ansible_t1

Первое задание. Деплой через простые плейбуки. Необходимо запускать плейбук с ключом -b

* reactjs_playbook.yml

* nginx.yml


## Ansible_t1

Второе задание. Деплой с использованием ролей.

В ролях учтена необходимость использование привилегированных прав:

* Для задач установка необходимых компонентов reactjs используется импорт с повышением прав:

```
- { include: install.yml, become: yes }
```

* Для запуска задач деплоя используется блок задач с повышением прав:

```
- block:

  - name: Set variable release_path
    set_fact:
      release_path: "{{ project_path }}/releases/{{ lookup('pipe','date +%Y%m%d%H%M%S') }}"

  ...

  - name: Start reactjs
    command: "pm2 --name ReactJS start npm -- start"
    args:
      chdir: "{{ release_path }}"
    environment:
      PORT: 80

  become: true
```

* В задачах nginx повышение прав используется для конкретных задач, где это требуется:

```
- name: Remove nginx default config
  file: 
    path: "{{ nginx_config_path }}"
    state: absent
  become: yes
```

