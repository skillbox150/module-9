module "common" {  
  source  = "./module_common"
}

#module "one-server" {  
#  source  = "./module_one-server"
#  subnet  = module.common.subnets.zone_b
#  img_id  = module.common.default_img.id
#}
#
#resource "local_file" "template_inventory" {
#  content     = templatefile("./OneServer/hosts.tpl", {
#      server_ip = module.one-server.my_web_site_ip
#  })
#  filename = "./OneServer/hosts"
#}
#
## Выведем IP адрес сервера
#output "my_web_site_ip" {
#  description = "Elatic IP address assigned to our WebSite"
#  value       = module.one-server.my_web_site_ip
#}

# два бэкэнда и балансировщик нагрузки
module "servers" {
  source  = "./module_servers"
  subnets  = module.common.subnets
  img_id  = module.common.default_img.id
}

# IP адрес балансировщика
output "balancer_ip" {
  description = "Адрес балансировщика"
  value       = module.servers.balancer_ip
}

# IP адрес react сервера
output "react_ip" {
  description = "Адрес балансировщика"
  value       = module.servers.react_ip
}

# Заполним инвентори файл
resource "local_file" "template_inventory_two-servers" {
  content     = templatefile("./hosts.tpl", {
    instances = module.servers.vm_group.instances
    react_ip = module.servers.react_ip
  })
  filename = "../hosts"
}
