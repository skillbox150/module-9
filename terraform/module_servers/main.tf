# читаем содержимое файла user.data
data "local_file" "user_data" {
    filename = "${path.module}/user.data"
}
data "local_file" "nginx_data" {
    filename = "${path.module}/nginx.data"
}

# создаем сервисный аккаунт
resource "yandex_iam_service_account" "sa" {
  name        = "sa"
  description = "service account to manage VMs"
}

# предоставляем сервисному аккаунту доступ с ролью editor в текущем каталоге
resource "yandex_resourcemanager_folder_iam_binding" "sa-iam" {
  folder_id   = "${var.folder_id}"
  role        = "editor"
  members     = [
    "serviceAccount:${yandex_iam_service_account.sa.id}",
  ]
  lifecycle {
    create_before_destroy = true
  }
}


# создаем группы виртуальных машин
resource "yandex_compute_instance_group" "nginx" {
  name = "nginx"
  folder_id = "${var.folder_id}"
  service_account_id = "${yandex_iam_service_account.sa.id}"
  instance_template {
    platform_id = "standard-v3"
    resources {
      cores         = 2
      core_fraction = 20
      memory        = 1
    }
    boot_disk {
      initialize_params {
        image_id = var.img_id
        size = 15
      }
    }
    network_interface {
      subnet_ids      = [
        var.subnets.zone_a.id, 
        var.subnets.zone_c.id
        ]
      nat             = true
    }
    metadata = {
      user-data = "${data.local_file.nginx_data.content}"
      serial-port-enable = "1"
    }
    service_account_id = "${yandex_iam_service_account.sa.id}"
  }
  scale_policy {
    fixed_scale {
      size = 2
    }
  }
  allocation_policy {
    zones = [
      var.subnets.zone_a.zone, 
      var.subnets.zone_c.zone
    ]
  }
  deploy_policy {
    max_unavailable = 2
    max_creating = 2
    max_expansion = 2
    max_deleting = 2
  }
  load_balancer {
    target_group_name = "nginx"
  }
  depends_on = [
    yandex_resourcemanager_folder_iam_binding.sa-iam
  ]
}

# целевая группа для балансера
#resource "yandex_lb_target_group" "two-servers" {
#  name      = "two-servers"
#
#  dynamic "target" {
#    for_each = yandex_compute_instance_group.two-servers.instances
#    content {
#      subnet_id = target.value.network_interface.0.subnet_id
#      address   = target.value.network_interface.0.ip_address
#    }
#  }
#}

resource "yandex_lb_network_load_balancer" "nginx-lb" {
  name = "nginx-lb"

  listener {
    name = "nginx-listener"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = "${yandex_compute_instance_group.nginx.load_balancer.0.target_group_id}"

    healthcheck {
      name = "http"
      http_options {
        port = 80
      }
    }
  }
}


# react server
resource "yandex_compute_instance" "react" {
  name = "react"
  allow_stopping_for_update = true

  resources {
    cores         = 2
    core_fraction = 20
    memory        = 1
  }

  network_interface {
    subnet_id       = var.subnets.zone_b.id
    nat             = true
  }

  # прерываемая
  scheduling_policy {
    preemptible = true
  }

  boot_disk {
    initialize_params {
      image_id = var.img_id
      size = 15
    }
  }

  metadata = {
    user-data = "${data.local_file.user_data.content}"
    serial-port-enable = "1"
  }
}

# Вернем группу ВМ
output "vm_group" {
  value = yandex_compute_instance_group.nginx
}

# Вернем ip балансера
output "balancer_ip" {
  value = yandex_lb_network_load_balancer.nginx-lb.listener.*.external_address_spec[0].*.address[0]
}

output "react_ip" {
  description = "Внешний адрес react сервера"
  value       = yandex_compute_instance.react.network_interface[0].nat_ip_address
}
