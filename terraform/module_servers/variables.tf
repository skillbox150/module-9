variable "subnets" {
  description = "Подсети для группы ВМ"
  type        = any
}

variable "img_id" {
  description = "Используемый образ"
  type        = string
}
